// Note for seeing accessibility events:
- Build and install the app for the first time
- Activate accessibility setting: Go to Settings > Accessibility > Demo Accessibility > Switch on
  Accessibility events will start to be caught (as in breakpoint line 23 of MyAccessibilityService.java)
- See events with any app:
    Open chrome browser and select some texts, breakpoint line 23 is triggered
    With Hi Translate app's quick translate floating button on, hover this button to any textview or edit text, breakpoint line 23 is triggered
 - To-do:
  Detect view Location
  Show annotation view as Hi Dictionary
  